package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import logic.VisitCharacteristics;
import objects.Accompanying;
import objects.ClinicWorker;
import objects.Pass;
import objects.Patient;
import objects.Record;
import objects.Visit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;


public class TestVisitCharacteristic {

	private ArrayList<Visit> list = new ArrayList<Visit>();
	private Pass passCard0, passCard1, passCard2, passCard3;
    
	@Before
    public void init(){
		passCard0 = new Pass(0, 1234, "Mihailov", 0);
		passCard1 = new Pass(1, 1235, "Ivanova", 0);
		passCard3 = new Pass(3, 1237, "Sidorov", 2);
		passCard2 = new Pass(4, 1238, "Sokolov", 3);
		
    }
    @After
    public void tearDown() { 
    	list = null;
    }
	
	
    public void fillList(){
    	Visit visit1 = new Visit(passCard0, "07.07.07 07:07", "07.07.07 17:07");
		Visit visit2 = new Visit(passCard1, "07.07.07 07:27", "07.07.07 17:27");
		Visit visit3 = new Visit(passCard3, "07.07.07 07:37", "07.07.07 17:37");
    	
    	list.add(visit1);
    	list.add(visit2);
    	list.add(visit3);
    }
    
	@Test
	public void testGetListOfVisit() {
		fillList();
		VisitCharacteristics ch = new VisitCharacteristics(list);
		
		//assertEquals("07.07.07 07:07", ch.getListOfVisit().get(0).getEntry());
		//assertEquals("07.07.07 17:27", ch.getListOfVisit().get(1).getExit());
		//assertEquals(passCard3, ch.getListOfVisit().get(2).getPass());
	}

	@Test
	public void testGetCurrentDate() {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		Date currentDate = new Date();
        SimpleDateFormat dateFormat = null;
        dateFormat = new SimpleDateFormat();
        
        assertEquals(dateFormat.format( currentDate ), ch.getCurrentDate());
	}

	@Test
	public void testCompareDate() throws ParseException {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		
		assertTrue(ch.compareDate("07.07.07 07:07"));
		assertTrue(ch.compareDate("07.07.07 17:47"));
	}

	@Test
	public void testCheckEntryWorker() throws ParseException {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		
		ClinicWorker worker0 = new ClinicWorker(passCard0, 1234, "Mihailov", true);
		assertTrue(ch.checkEntryWorker(worker0));
		//assertTrue(ch.checkEntryWorker(worker0));
	}

	@Test
	public void testCheckEntryPatient() {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		Date currentDate = new Date();
        SimpleDateFormat dateFormat = null;
        dateFormat = new SimpleDateFormat();
		Record record0 = new Record("07.07.07 07:07", "Hirurg");
		Patient patient0 = new Patient("Sidorov", passCard3, 1237, false);
        patient0.addRecord(record0);
        
        try {
			assertFalse(ch.checkEntryPatient(patient0));
		} catch (ParseException e) {
			Assert.fail(e.getMessage());
		}
        patient0.addRecord(new Record(dateFormat.format( currentDate ), "Hirurg"));
       // assertTrue(ch.checkEntryPatient(patient0));
		
	}

	@Test
	public void testCheckEntryPerson() {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		Date currentDate = new Date();
        SimpleDateFormat dateFormat = null;
        dateFormat = new SimpleDateFormat();
		Accompanying acc0 = new Accompanying("Medvedeva", passCard2, 5678);
        Patient patient2 = new Patient("Sokolov", passCard3, 1235, acc0, false, 
        		new Record(dateFormat.format( currentDate ),"Terapevt"));
        Patient patient0 = new Patient("Sidorov", passCard3, 1237, false);
        Record rec = new Record(dateFormat.format( currentDate ), "Hirurg");
        patient0.addRecord(rec);
        
        assertFalse(ch.checkEntryPerson(patient2));
        fillList();
        assertFalse(ch.checkEntryPerson(patient0));
        assertTrue(ch.checkEntryPerson(patient2));
	}

	@Test
	public void testCheckEntry() {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		Date currentDate = new Date();
        SimpleDateFormat dateFormat = null;
        dateFormat = new SimpleDateFormat();
		
		ClinicWorker worker0 = new ClinicWorker(passCard0, 1234, "Mihailov", true);
		Accompanying acc0 = new Accompanying("Medvedeva", passCard2, 5678);
		Patient patient2 = new Patient("Sokolov", passCard3, 1235, acc0, false, 
        		new Record(dateFormat.format( currentDate ),"Terapevt"));
        Patient patient0 = new Patient("Sidorov", passCard3, 1237, false);
        Record rec = new Record(dateFormat.format( currentDate ), "Hirurg");
        patient0.addRecord(rec);
		/* assertTrue(ch.checkEntry(passCard0, worker0, patient0));
		 assertTrue(ch.checkEntry(passCard1, worker0, patient0));
		 assertTrue(ch.checkEntry(passCard3, worker0, patient0));
		 assertTrue(ch.checkEntry(passCard2, worker0, patient2));
		 
		 assertTrue(ch.checkEntry(passCard0, worker0, patient0));
		 assertFalse(ch.checkEntry(passCard3, worker0, patient0));/*/
	}

	@Test
	public void testCheckExit() throws ParseException {
		VisitCharacteristics ch = new VisitCharacteristics(list);
		fillList();
		
		 assertTrue(ch.checkExit(passCard0));
		 assertTrue(ch.checkExit(passCard1));
		 assertTrue(ch.checkExit(passCard3));
	}

	@Test
	public void testExistPass() {
		fillList();
		VisitCharacteristics ch = new VisitCharacteristics(list);
		
		assertTrue(ch.existPass(passCard0));
		assertTrue(ch.existPass(passCard1));
		assertTrue(ch.existPass(passCard3));
		assertFalse(ch.existPass(passCard2));
	}
	
	public static void main(String[] args) throws Exception {
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(TestPass.class);
        System.out.println("run tests: " + result.getRunCount());
        System.out.println("failed tests: " + result.getFailureCount());
        System.out.println("ignored tests: " + result.getIgnoreCount());
        System.out.println("success: " + result.wasSuccessful());
    }

}
