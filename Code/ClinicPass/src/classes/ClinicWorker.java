package classes;

import java.util.HashSet;
import java.util.Set;


public class ClinicWorker {
	
	private Pass passID;
	private int passport;
	private String FIO;
	private boolean flag = false;
	private Set<Patient> patients = new HashSet<Patient>();
	
	public ClinicWorker(Pass p, int pp, String fio, boolean f){
		passID = p;
		passport = pp;
		FIO = fio;
		flag = true;
	}
	
	public ClinicWorker(Pass p, int pp, String f){
		passID = p;
		passport = pp;
		FIO = f;
		flag = false;
	}
	
	public Pass getPassID(){
		return passID;
	}
	
	public void setPassID(Pass newPassID){
		passID = newPassID;
	}
	
	public int getPassport(){
		return passport;
	}
	
	public void setPassport(int newPassport){
		passport = newPassport;
	}
	
	public String getFIO(){
		return FIO;
	}
	
	public void setFIO(String newFIO){
		FIO = newFIO;
	}
	
	public void addPatient(Patient newPatient){
		patients.add(newPatient);
        // связываем сотрудника с этим отделом
        newPatient.setClinicWorker(this);
    }
    public Set<Patient> getPatient(){
        return patients;
    }
    public void removePatient(Patient p){
    	patients.remove(p);
    }
    
    public boolean getFlag(){
		return flag;
	}
	
	public void setFlag(boolean newFlag){
		flag = newFlag;
	}

}
