package classes;

public class Pass {
	
	private int passID;
	private int passport;
	private String FIO;
	private int status;					//0 --- worker, 1 --- bad patient, 
										//2 --- patient, 3 --- person with patient
	
	public Pass(int p, int pp, String f, int s){
		passID = p;
		passport = pp;
		FIO = f;
		status = s;
	}
	
	public Pass(int p, int s){
		passID = p;
		status = s;
	}
	
	public int getPassID(){
		return passID;
	}
	
	public void setPassID(int newPassID){
		passID = newPassID;
	}
	
	public int getPassport(){
		return passport;
	}
	
	public void setPassport(int newPassport){
		passport = newPassport;
	}
	
	public String getFIO(){
		return FIO;
	}
	
	public void setFIO(String newFIO){
		FIO = newFIO;
	}
	
	public int getStatus(){
		return status;
	}
	
	public void setStatus(int newStatus){
		status = newStatus;
	}
	
	public boolean compare(Pass p1, Pass p2){
		boolean res;
		if( (p1.passID == p2.passID) && (p1.status == p2.status))
			res = true;
		else
			res = false;
		return res;
	}
}
