package classes;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class Visit {

	/**
	 * @param args
	 */
	private Date entry;
	private Date exit;
	private boolean gate;
	private Map<Pass, Date> passes = new HashMap<Pass, Date>();
	
	public Date getEntry(){
		return entry;
	}
	
	public void setEntry(Date newEntry){
		entry = newEntry;
	}
	
	public Date getExit(){
		return exit;
	}
	
	public void setExit(Date newExit){
		exit  = newExit;
	}
	
	public boolean getGate(){
		return gate;
	}
	
	public void setGate(boolean newGate){
		gate = newGate;
	}
	
	public  Map<Pass, Date> getPasses(){
		return passes;
	}
	
	public boolean checkEntryWorker(ClinicWorker worker){
		
		boolean check = false;
		entry = new Date();
		
		if(worker.getFlag()){
			int id = worker.getPassID().getPassID();
			Pass pass = new Pass(id, 1);
			Patient patient = new Patient(pass,true);
			worker.addPatient(patient);
			passes.put(pass, entry);
		}
		
		
		//System.out.println("entry: " + entry);
		passes.put(worker.getPassID(), entry);
		check = true;
		return check;
	}
	
	public boolean checkExitWorker(ClinicWorker worker){
		boolean check = false;
		exit = new Date();
		//System.out.println("exit: " + exit);
		
		if(passes.containsKey(worker.getPassID()) &&  passes.get(worker.getPassID()).before(exit)){
			passes.put(worker.getPassID(), exit);
			check = true;
		}
	
		return check;
	}
	
@SuppressWarnings({ "deprecation" })
	public boolean checkEntryPatient(Patient patient){
		
		boolean check = false;
		entry = new Date();
	
		if(passes.containsKey(patient.getPassID())){
			//throw new IllegalArgumentException("Already entered"); 
		}
		else{
			if(!patient.getState()){
				Set<Record> p = patient.getRecords();
				Iterator<Record> iterator = p.iterator();
				if(p.isEmpty())
					throw new IllegalArgumentException("No records"); 
				else
					while (iterator.hasNext()){
						if(iterator.next().getDate().getDay() == entry.getDay()){
							check = true;
							passes.put(patient.getPassID(), entry);
						}
					}
			}
		
			else{
				check = true;
				passes.put(patient.getPassID(), entry);
			}
		}
		return check;
	}

	public boolean checkEntryPerson(Patient patient, Accompanying acc){
		boolean check = false;
		entry = new Date();
		
		
		//System.out.println("IDAcc: "+ acc.getPassID().getPassID());
		//System.out.println("IDPatient: " + patient.getPassID().getPassID());
		if(patient.getPassID().getPassID() == acc.getPassID().getPassID()){
			if(passes.containsKey(patient.getPassID())){
				passes.put(acc.getPassID(), entry);
				check = true;
			}
		}
		//else 
			//throw new IllegalArgumentException("No person with patient was registrated");

		return check;
	}

	public boolean checkEntryVisitor(Pass pass){
		boolean check = false;
		int c = pass.getStatus();
		
		switch(c){
			case 0: break;
			case 1: break;
			case 2: break;
			case 3: break;
		}

		return check;
	}
	
	public boolean checkExit(Pass pass) {
		boolean check = false;
		exit = new Date();

		if(passes.containsKey(pass)){
			if(passes.get(pass).before(exit))
				check = true;
			//else
				//throw new IllegalArgumentException("No person was entered");
		}
		return check;
	}
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Visit visit = new Visit();
		Map<Pass, Date> passes = visit.getPasses();
		Pass passCard0 = new Pass(0, 1234, "Mihailov", 0);
		Pass passCard1 = new Pass(0, 1235, "Ivanova", 0);
		Pass passCard3 = new Pass(3, 1237, "Sidorov", 2);
		Pass passCard4 = new Pass(3, 1238, "Sokolov", 3);
		ClinicWorker worker0 = new ClinicWorker(passCard0, 1234, "Mihailov");
		ClinicWorker worker1 = new ClinicWorker(passCard1, 1234, "Mihailov");
		Record record0 = new Record(new Date(), "Hirurg");
	    Patient patient0 = new Patient("Sidorov", passCard0, 1234, false, record0);
	    Accompanying acc0 = new Accompanying("Medvedeva", passCard4, 5678);
	    Patient patient2 = new Patient("Sokolov", passCard3, 1235, acc0, false, new Record(new Date(),"Terapevt"));
		
		//visit.checkEntryWorker(worker0);
		//TimeUnit.SECONDS.sleep(2);
		
	    visit.checkEntryPatient(patient2);
	    		
		System.out.println("??? " + visit.checkEntryPerson(patient2, acc0));
		
		
		/*for (Map.Entry entry : passes.entrySet()) {
		    System.out.println("Key: " + entry.getKey() + " Value: "
		        + entry.getValue());
		}*/
		/*Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		Date d = cal.getTime();
		Date dd = new Date();
		System.out.println("??? " + d + "/t" + dd);*/

	}

}
