package classes;

import java.util.LinkedHashSet;
import java.util.Set;


public class Patient extends Visitor{
	
	private int passport;
	private Accompanying person;
	private boolean state;								// false ---good, true --- bad	
	private Set<Record> records = new LinkedHashSet<Record>();
    private ClinicWorker worker;
	
	public Patient(String s, Pass p, int doc, Accompanying acc, boolean st, Record rec){ // норм пациент с сопровождением
		FIO = s;
		passID = p;
		doc = passport;
		person = acc;
		state = st;
		addRecord(rec);
	}
	
	public Patient(String s, Pass p, int doc, boolean st, Record rec){ // норм пациент
		FIO = s;
		passID = p;
		doc = passport;
		state = st;
		addRecord(rec);
	}
	
	public Patient(Pass p, boolean st){ //пациент в тяжелом состоянии
		passID = p;
		state = st;
	}
	
	public int getPassport(){
		return passport;
	}
	
	public void setPassport(int newPassport){
		passport = newPassport;
	}
	
	public Accompanying getPerson(){
		return person;
	}
	
	public void setPerson(Accompanying newPerson){
		person = newPerson;
	}
	
	public boolean getState(){
		return state;
	}
	
	public void setState(boolean newState){
		state = newState;
	}
	
	public void addRecord(Record newRecord){
        records.add(newRecord);
        // связываем сотрудника с этим отделом
        newRecord.setPatient(this);
    }
    public Set<Record> getRecords(){
        return records;
    }
    public void removeRecord(Record r){
        records.remove(r);
    }
    
	public void setClinicWorker(ClinicWorker w){
		worker = w;
	}
	public ClinicWorker getClinicWorker(){
	    return worker;
	}

}
