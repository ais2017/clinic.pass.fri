package interfaceW;

import java.util.ArrayList;

import objects.Visit;

public interface InterfaceGateway {
	public int getPass();
	public void sendGate(boolean g);
	public void sendList(ArrayList<Visit> list);
}
