package exaption;

@SuppressWarnings("serial")
public class PatientNotExistExeption extends Exception{
	public PatientNotExistExeption(String message) {
        super(message);
    }
}