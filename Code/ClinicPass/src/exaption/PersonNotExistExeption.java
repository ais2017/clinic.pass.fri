package exaption;

@SuppressWarnings("serial")
public class PersonNotExistExeption extends Exception{
	public PersonNotExistExeption(String message) {
        super(message);
    }
}
