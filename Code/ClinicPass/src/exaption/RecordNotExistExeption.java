package exaption;

@SuppressWarnings("serial")
public class RecordNotExistExeption  extends Exception{
	public RecordNotExistExeption(String message) {
        super(message);
    }
}
