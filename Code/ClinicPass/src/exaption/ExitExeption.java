package exaption;

@SuppressWarnings("serial")
public class ExitExeption extends Exception{
	public ExitExeption(String message) {
        super(message);
    }
}