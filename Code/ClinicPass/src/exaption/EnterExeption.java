package exaption;

@SuppressWarnings("serial")
public class EnterExeption extends Exception{
	public EnterExeption(String message) {
        super(message);
    }
}