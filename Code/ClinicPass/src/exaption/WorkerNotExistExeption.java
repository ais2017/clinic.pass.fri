package exaption;

@SuppressWarnings("serial")
public class WorkerNotExistExeption extends Exception{
	public WorkerNotExistExeption(String message) {
        super(message);
    }
}
