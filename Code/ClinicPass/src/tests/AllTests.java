package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestScenarios.class, TestVisitCharacteristic.class, VisitTest.class })
public class AllTests {

}
