package tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import interfaceW.InterfaceGatewayImpl;

import java.sql.SQLException;

import logic.Scenarios;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestScenarios {
	

	@Before
	public void init(){

	}
	
	@Test
	public void testEntry() throws ClassNotFoundException, SQLException {
		InterfaceGatewayImpl iface = new InterfaceGatewayImpl();
		Scenarios sa = new Scenarios(iface);
		try{
			iface.setID(1);
			assertTrue(sa.entry());
			iface.setID(2);
			assertTrue(sa.entry());
			iface.setID(3);
			assertTrue(sa.entry());
			iface.setID(4);
			assertFalse(sa.entry());
			iface.setID(6);
			assertTrue(sa.entry());
			iface.setID(4);
			assertTrue(sa.entry());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	//@Ignore
	@Test
	public void testExit() throws ClassNotFoundException, SQLException {
		InterfaceGatewayImpl iface = new InterfaceGatewayImpl();
		Scenarios sa = new Scenarios(iface);
		try{
			iface.setID(1);
			assertTrue(sa.entry());
			assertTrue(sa.exit());
			
			iface.setID(6);
			assertFalse(sa.exit());
		}catch (Exception e) {
            Assert.fail(e.getMessage());
        }
	}


}
