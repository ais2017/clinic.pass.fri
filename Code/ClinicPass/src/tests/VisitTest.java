package tests;

import static org.junit.Assert.*;
import objects.Pass;
import objects.Visit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class VisitTest {
	
	private Pass passCard0, passCard1, passCard2, passCard3;
    
	@Before
    public void init(){
		passCard0 = new Pass(0, 1234, "Mihailov", 0);
		passCard1 = new Pass(1, 1235, "Ivanova", 0);
		passCard2 = new Pass(2, 1236, "Sidorov", 1);
		passCard3 = new Pass(3, 1237, "Sidorov", 2);
    }
	
    @After
    public void tearDown() { 
    	passCard0 = null;
    	passCard1 = null;
    	passCard2 = null;
    	passCard3 = null;
    }
    
	@Test
	public void testGetPass() {
		Visit visit1 = new Visit(passCard0);
		Visit visit2 = new Visit(passCard1);
		Visit visit3 = new Visit(passCard2);
		Visit visit4 = new Visit(passCard3);
		
		assertEquals(passCard0, visit1.getPass());
		assertEquals(passCard1, visit2.getPass());
		assertEquals(passCard2, visit3.getPass());
		assertEquals(passCard3, visit4.getPass());
	}

	@Test
	public void testSetPass() {
		Visit visit = new Visit(null);
		visit.setPass(passCard0);
		assertEquals(passCard0, visit.getPass());
		visit.setPass(passCard1);
		assertEquals(passCard1, visit.getPass());
		visit.setPass(passCard2);
		assertEquals(passCard2, visit.getPass());
	}

	@Test
	public void testGetEntry() {
		Visit visit = new Visit(passCard0);
		visit.setEntry("07.02.14 15:44");
		assertEquals("07.02.14 15:44", visit.getEntry());
		visit.setEntry("07.07.14 17:34");
		assertEquals("07.07.14 17:34", visit.getEntry());
		visit.setEntry("07.02.12 15:34");
		assertEquals("07.02.12 15:34", visit.getEntry());
	}

	@Test
	public void testGetExit() {
		Visit visit = new Visit(passCard0);
		visit.setExit("07.02.14 15:44");
		assertEquals("07.02.14 15:44", visit.getExit());
		visit.setExit("07.07.14 17:34");
		assertEquals("07.07.14 17:34", visit.getExit());
		visit.setExit("07.02.12 15:34");
		assertEquals("07.02.12 15:34", visit.getExit());
	}

	@Test
	public void testGetGate() {
		Visit visit = new Visit(null);
		visit.setGate(true);
		assertTrue(visit.getGate());
		visit.setGate(false);
		assertFalse(visit.getGate());
	}

	@Test
	public void testEqualsObject() {
		Visit visit1 = new Visit(passCard0);
		Visit visit2 = new Visit(passCard1);
		Visit visit3 = new Visit(passCard0);
		
		assertFalse(visit1.equals(visit2));
		assertTrue(visit3.equals(visit1));
		assertFalse(visit2.equals(visit3));
		
	}
	
	public static void main(String[] args) throws Exception {
        JUnitCore runner = new JUnitCore();
        Result result = runner.run(TestPass.class);
        System.out.println("run tests: " + result.getRunCount());
        System.out.println("failed tests: " + result.getFailureCount());
        System.out.println("ignored tests: " + result.getIgnoreCount());
        System.out.println("success: " + result.wasSuccessful());
    }

}
