package database;

import java.sql.SQLException;
import java.util.ArrayList;

import objects.ClinicWorker;
import objects.Pass;
import objects.Patient;
import objects.Visit;

public interface DBGateway {
	
	Pass getPass(int pass) throws SQLException, ClassNotFoundException, Exception;
	ClinicWorker getWorker(Pass pass) throws Exception;
	Patient getPatient(Pass pass) throws SQLException, ClassNotFoundException,Exception;
	Patient getPerson(Pass pass) throws ClassNotFoundException, SQLException,Exception;
	boolean saveVisit (ArrayList<Visit> list) throws SQLException, ClassNotFoundException,Exception;

}
