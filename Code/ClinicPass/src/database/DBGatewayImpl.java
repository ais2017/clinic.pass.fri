package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import objects.Accompanying;
import objects.ClinicWorker;
import objects.Pass;
import objects.Patient;
import objects.Record;
import objects.Visit;

public class DBGatewayImpl implements DBGateway {

	public ClinicWorker getWorker(Pass pass) throws Exception {
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		   System.out.println("База Подключена!");	
		   
		String sql = "SELECT id_w, fio_w FROM worker WHERE id_pass = ?";
		PreparedStatement pstmt  = conn.prepareStatement(sql);
		// set the value
        pstmt.setInt(1, pass.getPassID());
		ResultSet resSet = pstmt.executeQuery();
		
		int id = 0; String fio = null;
			
			while(resSet.next())
			{
				id = resSet.getInt("id_w");
				fio = resSet.getString("fio_w");
			}	
			
	         System.out.println( "name = " + fio );
	         System.out.println( "pass = " + id  );
			System.out.println("Таблица выведена");
			
			ClinicWorker worker = new ClinicWorker(pass, id, fio);
		if(fio == null)
			throw new Exception("Нет такого сотрудника");
		
		resSet.close();
		pstmt.close();
		conn.close();
	
		return worker;
		//ClinicWorker worker = new ClinicWorker(pass, 1234, "Mihailov");
		//return worker;
	}

	public Pass getPass(int pass) throws Exception {
		
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		   System.out.println("База Подключена!");	
		   
		String sql = "SELECT * FROM pass WHERE id_pass = ?";
		PreparedStatement pstmt  = conn.prepareStatement(sql);
		// set the value
        pstmt.setInt(1, pass);
		ResultSet resSet = pstmt.executeQuery();
		
		int id = 0; int p = 0; int st = 7; String fio = null;
			
			while(resSet.next())
			{
				id = resSet.getInt("id_pass");
				p = resSet.getInt("id");
				fio = resSet.getString("fio");
				st = resSet.getInt("status_p");
			}	
			
	         System.out.println( "ID = " + p );
	         System.out.println( "name = " + fio );
	         System.out.println( "pass = " + id  );
	         System.out.println( "status" + st);
			System.out.println("Таблица выведена");
			
		Pass passCard = new Pass(id, p, fio, st);
		if(fio == null)
			throw new Exception("Нет такого пропуска");
		
		resSet.close();
		pstmt.close();
		conn.close();
		//Pass passCard = new Pass(id, 1234, "Mihailov", 0);
			
		
		return passCard;
	}
	
	public Patient getPatient(Pass pass) throws SQLException, ClassNotFoundException {
		
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		   System.out.println("База Подключена!");	
		   
		String sql = "SELECT * FROM patient, record WHERE (id_pass = ? AND id_pr = id_p)";
		PreparedStatement pstmt  = conn.prepareStatement(sql);

        pstmt.setInt(1, pass.getPassID());
		ResultSet resSet = pstmt.executeQuery();
		int id = 0; String fio = null; boolean st=false;
		String date = null; String doctor = null; 
		Patient newPatient = null;//new Patient(fio, pass, id, st);
			
			while(resSet.next())
			{
				id = resSet.getInt("id_p");
				fio = resSet.getString("fio_p");
				if(resSet.getInt("state")==0)
					st = false;
				else
					st = true;
				newPatient = new Patient(fio, pass, id, st);
				date = resSet.getString("date_r");
				doctor = resSet.getString("doctor");
				Record record = new Record(date, doctor);
				newPatient.addRecord(record);
				System.out.println("ADD: "+ record.getDate());
			}	
			
	        System.out.println( "name = " + fio );
	        System.out.println( "doctor = " + doctor );
	        System.out.println( "state = " + st );
			System.out.println("Таблица выведена");
			
		
		resSet.close();
		pstmt.close();
		conn.close(); 

		return newPatient;
	}


	public Patient getPerson(Pass pass) throws ClassNotFoundException, SQLException {
		
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		   System.out.println("База Подключена!");	
		   
		String sql = "SELECT * FROM patient, person WHERE person.id_pass = ? AND patient.id_p = person.id_p";
		PreparedStatement pstmt  = conn.prepareStatement(sql);
		// set the value
        pstmt.setInt(1, pass.getPassID());
		ResultSet resSet = pstmt.executeQuery();
		
		
		int id_a=0; String fio_a = null;
		int id = 0; String fio = null; boolean st=false;
		Patient newPatient = null;
		
			while(resSet.next())
			{
				id = resSet.getInt("id_p");
				fio = resSet.getString("fio_p");
				if(resSet.getInt("state")==0)
					st = false;
				else
					st = true;
				id_a = resSet.getInt("id_ac");
				fio_a = resSet.getString("fio_ac");
				
			}	
			
	         System.out.println( "ID = " + id );
	         System.out.println( "name = " + fio );
	         System.out.println( "namePerson = " + fio_a );
			System.out.println("Таблица выведена");
			
			newPatient = new Patient(fio, pass, id, st);
			newPatient.setPerson(new Accompanying(fio_a, pass, id_a));
		
		resSet.close();
		pstmt.close();
		conn.close();
		
		return newPatient;
	}

	public boolean saveVisit(ArrayList<Visit> list) throws SQLException, ClassNotFoundException, Exception {
		Connection conn = null;
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		System.out.println("База Подключена!");	
		Statement statmt = conn.createStatement();  
		
		statmt.execute("DELETE FROM visit; ");
		
		String sql = "INSERT INTO visit(id_v, name, enter, exit) VALUES(?,?,?,?)";
		
		for(int i =0; i< list.size(); i++){
			PreparedStatement pstmt = conn.prepareStatement(sql);
	            pstmt.setInt(1, list.get(i).getPass().getPassport());
	            pstmt.setString(2, list.get(i).getPass().getFIO());
	            pstmt.setString(3, list.get(i).getEntry());
	            pstmt.setString(4, list.get(i).getExit());
	            pstmt.executeUpdate();
		}
		
	
		System.out.println("SAVE");
		return false;
	}

}
