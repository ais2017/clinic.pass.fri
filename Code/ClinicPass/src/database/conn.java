package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class conn {
	
	public static Connection conn;
	public static Statement statmt;
	public static ResultSet resSet;
	
	// --------ПОДКЛЮЧЕНИЕ К БАЗЕ ДАННЫХ--------
	public static void Conn() throws ClassNotFoundException, SQLException 
	   {
		   conn = null;
		   Class.forName("org.sqlite.JDBC");
		   conn = DriverManager.getConnection("jdbc:sqlite:CLINIC.s3db");
		   
		   System.out.println("База Подключена!");
	   }
	
	// --------Создание таблицы--------
	public static void CreateDB() throws ClassNotFoundException, SQLException
	   {
		statmt = conn.createStatement();
		//statmt.execute("CREATE TABLE if not exists 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'name' text, 'phone' INT);");
		
				//-----------Worker-----------//
				statmt.execute("CREATE TABLE if not exists 'worker' " +
						"('id_w' INTEGER PRIMARY KEY AUTOINCREMENT, " +
						"'fio_w' text," +
						"'id_pass' INTEGER," +
						"'st' INTEGER);");
				
				//-----------Patient-----------//
				statmt.execute("CREATE TABLE if not exists 'patient' " +
						"('id_p' INTEGER PRIMARY KEY AUTOINCREMENT," +
						" 'fio_p' text," +
						"'id_pass' INTEGER," +
						" 'state'  INTEGER);");
						
				//-----------Person-----------//
				statmt.execute("CREATE TABLE if not exists 'person' " +
						"('id_ac' INTEGER PRIMARY KEY AUTOINCREMENT, " +
						"'fio_ac' text," +
						"'id_pass' INTEGER," +
						"'id_p' INTEGER);");
				
				//-----------Record-----------//
				statmt.execute("CREATE TABLE if not exists 'record' " +
						"('id_pr' INTEGER, " +
						"'date_r' text, " +
						"'doctor' text," +
						"FOREIGN KEY (id_pr) REFERENCES patient(id_p));");
				
				//-----------Pass-----------//
				statmt.execute("CREATE TABLE if not exists 'pass' " +
						"('id_pass' INTEGER PRIMARY KEY AUTOINCREMENT, " +
						"'id' INTEGER," +
						"'fio' text," +
						"'status_p' INTEGER);");
				
				//-----------Visit-----------//
				statmt.execute("CREATE TABLE if not exists 'visit' " +
						"('id_v' INTEGER, " +
						"'enter' text," +
						"'exit' text," +
						"'gate' INTEGER," +
						"FOREIGN KEY (id_v) REFERENCES pass(id_pass));");
		
		
		
		System.out.println("Таблица создана или уже существует.");
	   }
	
	// --------Заполнение таблицы--------
	public static void WriteDB() throws SQLException
	{
		statmt.execute("INSERT INTO 'worker' ('fio_w', 'id_pass', 'st') VALUES ('Mihailov', 1, 0); ");
		statmt.execute("INSERT INTO 'patient' ('fio_p', 'id_pass', 'state') VALUES ('Sidorov', 2, 0); ");
		statmt.execute("INSERT INTO 'patient' ('fio_p', 'id_pass', 'state') VALUES ('Ivanova', 3, 0); ");
		statmt.execute("INSERT INTO 'person' ('fio_ac', 'id_pass', 'id_p') VALUES ('Petrov', 4, 3); ");
		statmt.execute("INSERT INTO 'record' ('id_pr', 'date_r', 'doctor') VALUES ('1', '07.07.07 07:07', 'Hirurg'); ");
		statmt.execute("INSERT INTO 'record' ('id_pr', 'date_r', 'doctor') VALUES ('2', '27.12.17 07:07', 'Zubnoy'); ");
		
		statmt.execute("INSERT INTO 'pass' ('id', 'fio', 'status_p') VALUES ('1', 'Mihailov', 0); ");
		statmt.execute("INSERT INTO 'pass' ('id', 'fio', 'status_p') VALUES ('1', 'Sidorov', 2); ");
		statmt.execute("INSERT INTO 'pass' ('id', 'fio', 'status_p') VALUES ('2', 'Ivanova', 2); ");
		statmt.execute("INSERT INTO 'pass' ('id', 'fio', 'status_p') VALUES ('1', 'Petrov', 3); ");
		//statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Olya', 222222); ");
		   //statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Petya', 125453); ");
		   //statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Vasya', 321789); ");
		   //statmt.execute("INSERT INTO 'users' ('name', 'phone') VALUES ('Masha', 456123); ");
		  
		   System.out.println("Таблица заполнена");
	}
	
	// -------- Вывод таблицы--------
	public static void ReadDB() throws ClassNotFoundException, SQLException
	   {
		resSet = statmt.executeQuery("SELECT * FROM worker");
		
		while(resSet.next())
		{
			int id = resSet.getInt("id_w");
			String  name = resSet.getString("fio_w");
			String  phone = resSet.getString("id_pass");
	         System.out.println( "ID = " + id );
	         System.out.println( "name = " + name );
	         System.out.println( "phone = " + phone );
	         System.out.println();
		}	
		
		System.out.println("Таблица выведена");
	    }
	
		// --------Закрытие--------
		public static void CloseDB() throws ClassNotFoundException, SQLException
		   {
			resSet.close();
			statmt.close();
			conn.close();
			
			System.out.println("Соединения закрыты");
		   }

}
