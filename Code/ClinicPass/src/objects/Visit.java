package objects;


public class Visit {

	private String entry;
	private String exit;
	private boolean gate;
	private Pass pass;
	
	public Visit(Pass newPass){
		this.gate = false;
		pass = newPass;
	}
	
	public Visit(Pass pass, String entry, String exit){
		this.pass = pass;
		this.entry = entry;
		this.exit = exit;
		this.gate = false;
	}
	
	public Pass getPass(){
		return pass;
	}
	
	public void setPass(Pass newPass){
		pass = newPass;
	}
	
	public String getEntry(){
		return entry;
	}
	
	public void setEntry(String newEntry){
		entry = newEntry;
	}
	
	public String getExit(){
		return exit;
	}
	
	public void setExit(String newExit){
		exit  = newExit;
	}
	
	public boolean getGate(){
		return gate;
	}
	
	public void setGate(boolean newGate){
		gate = newGate;
	}
	
	public boolean equals(Object obj)
	  {
	    if(obj == this)
	    return true;

	     /* obj ссылается на null */

	    if(obj == null)
	    return false;

	     /* Удостоверимся, что ссылки имеют тот же самый тип */

	    if(!(getClass() == obj.getClass()))
	    return false;
	    else
	    {
	      Visit tmp = (Visit)obj;
	      if(tmp.pass.getPassport() == this.pass.getPassport())
	       return true;
	      else
	       return false;
	    }
	  }
	
	

}
