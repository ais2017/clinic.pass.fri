package objects;

public class Record {

	private String date;
	private String doctor;
	private Patient patient;
	
	public Record(String d, String doc){
		this.date = d;
		this.doctor = doc;
	}
	
	public void setPatient(Patient p){
		patient = p;
	}
	public Patient getPatient(){
	    return patient;
	}
	
	public String getDate(){
		return date;
	}
	
	public void setDate(String newDate){
		date = newDate;
	}
	
	public String getDoctor(){
		return doctor;
	}
	
	public void setDoctor(String newDoctor){
		doctor = newDoctor;
	}
}
