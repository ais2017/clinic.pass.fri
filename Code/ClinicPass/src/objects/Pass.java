package objects;

public class Pass {
	
	private int passID;
	private int passport;
	private String FIO;
	private int status;					//0 --- worker, 1 --- bad patient, 
										//2 --- patient, 3 --- person with patient
	
	public Pass(int p, int pp, String f, int s){
		this.passID = p;
		this.passport = pp;
		this.FIO = f;
		this.status = s;
	}
	
	public Pass(int p, int s){
		this.passID = p;
		this.status = s;
	}
	
	public int getPassID(){
		return passID;
	}
	
	public void setPassID(int newPassID){
		passID = newPassID;
	}
	
	public int getPassport(){
		return passport;
	}
	
	public void setPassport(int newPassport){
		passport = newPassport;
	}
	
	public String getFIO(){
		return FIO;
	}
	
	public void setFIO(String newFIO){
		FIO = newFIO;
	}
	
	public int getStatus(){
		return status;
	}
	
	public void setStatus(int newStatus){
		status = newStatus;
	}
	
	public boolean compare(Pass p1, Pass p2){
		boolean res;
		if( (p1.passID == p2.passID) && (p1.status == p2.status))
			res = true;
		else
			res = false;
		return res;
	}
	
	public boolean equals(Object obj)
	  {
	    if(obj == this)
	    return true;

	     /* obj ссылается на null */

	    if(obj == null)
	    return false;

	     /* Удостоверимся, что ссылки имеют тот же самый тип */

	    if(!(getClass() == obj.getClass()))
	    return false;
	    else
	    {
	      Pass tmp = (Pass)obj;
	      if(tmp.getPassport() == this.getPassport())
	       return true;
	      else
	       return false;
	    }
	  }
}
