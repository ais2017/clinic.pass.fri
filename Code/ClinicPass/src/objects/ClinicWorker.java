package objects;

import java.util.HashSet;
import java.util.Set;


public class ClinicWorker {
	
	private Pass passID;
	private int passport;
	private String FIO;
	private boolean flag = false;                          //true --- with bad patient
	private Set<Patient> patients = new HashSet<Patient>();
	
	public ClinicWorker(Pass p, int pp, String fio, boolean f){
		this.passID = p;
		this.passport = pp;
		this.FIO = fio;
		this.flag = f;
	}
	
	public ClinicWorker(Pass p, int pp, String f){
		this.passID = p;
		this.passport = pp;
		this.FIO = f;
		this.flag = false;
	}
	
	public Pass getPassID(){
		return passID;
	}
	
	public void setPassID(Pass newPassID){
		passID = newPassID;
	}
	
	public int getPassport(){
		return passport;
	}
	
	public void setPassport(int newPassport){
		passport = newPassport;
	}
	
	public String getFIO(){
		return FIO;
	}
	
	public void setFIO(String newFIO){
		FIO = newFIO;
	}
	
	public void addPatient(Patient newPatient){
		patients.add(newPatient);
        // связываем пациента с этим 
        newPatient.setClinicWorker(this);
    }
    public Set<Patient> getPatient(){
        return patients;
    }
    public void removePatient(Patient p){
    	patients.remove(p);
    }
    
    public boolean getFlag(){
		return flag;
	}
	
	public void setFlag(boolean newFlag){
		flag = newFlag;
	}

}
