package logic;

import interfaceW.InterfaceGatewayImpl;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import objects.ClinicWorker;
import objects.Pass;
import objects.Patient;
import objects.Visit;
import database.DBGatewayImpl;
import exaption.*;

public class Scenarios {
	
	private VisitCharacteristics ch;
	private DBGatewayImpl db;
	private InterfaceGatewayImpl iface;
	
	public Scenarios (InterfaceGatewayImpl ifaceNew){
		this.iface = ifaceNew;
		this.ch = new VisitCharacteristics(new ArrayList<Visit>());
		this.db = new DBGatewayImpl();
	}
	
	public boolean entry() throws Exception{
		Pass pass = db.getPass(iface.getPass());
		int c = pass.getStatus();
		
		switch(c){
			case 0:
					ClinicWorker worker = db.getWorker(pass);
					if(worker == null)
						throw new Exception("Нет такого сотрудника");
					System.out.println("Worker");
					if(ch.checkEntryWorker(worker)){
						iface.sendGate(true);
						ch.showList();
						return true;
					}
					else{
						iface.sendGate(false);
					}
				
			case 2:
				Patient patient = db.getPatient(pass);
				if(patient == null)
					throw new Exception("Нет такого пациента");
				System.out.println("Patient");
					if(ch.checkEntryPatient(patient)){
						iface.sendGate(true);
						return true;
					}
					else{
						iface.sendGate(false);
					}
				
			case 3:
				Patient patientA = db.getPerson(pass);
				if(patientA == null)
					throw new Exception("Нет такого сопровождающего");
				System.out.println("Person");
					if(ch.checkEntryPerson(patientA)){
						iface.sendGate(true);
						return true;
					}
					else{
						iface.sendGate(false);
					}
		}
		return false;
	}
	
	public boolean exit() throws Exception{
		Pass pass = db.getPass(iface.getPass());
		boolean b = ch.checkExit(pass);
			if(b){
				iface.sendGate(b);
				ch.showList();
				return true;
			}
		
		return false;
	}
	
	public String [][] getList(){
		return ch.getListOfVisit();
	}
	
	public boolean saveList() throws ClassNotFoundException, SQLException, Exception{
		if(db.saveVisit(ch.getList()))
			return true;
		else 
			return false;
	}
	
}
